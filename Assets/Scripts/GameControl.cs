﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameControl : MonoBehaviour {

    public static GameControl instance;
    public GameObject gameOverText;
    public bool gameOver = false;

    public float scrollSpeed = -1.5f;
    public Text scoreText;

    public AudioClip AC;



    public int HighScore { get; protected set; }
    private int score = 0;

public int Score {
        get {
            return score;
        }

        protected set
        {
            // si la nouvelle valeur est differente de l'ancienne valeur
            if (value != score)
            {
                score = value;
            }
            if (score > HighScore) {
                HighScore = score;
                PlayerPrefs.SetInt("HighScore", HighScore); 
            }

        }
    }

	// Use this for initialization
	void Awake () {
        if (instance == null){
            instance = this;
        }
        else if (instance != this) {
            Destroy(gameObject); 
        }
        // On recupere le highscore du cache
        HighScore = PlayerPrefs.GetInt("HighScore", 0); 
	}
	
	// Update is called once per frame
	void Update () {
        if (gameOver == true && Input.GetMouseButtonDown(0) ){
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex ); 
        }
	}

    public void BirdScored(){
        if (gameOver){
            return;
        }
        Score++;
        AudioSource.PlayClipAtPoint(AC, transform.localPosition); 
        scoreText.text = "Score : " + Score.ToString(); 
    }


    public void BirdDied(){
        gameOverText.SetActive(true);
        gameOver = true;

    }


}
